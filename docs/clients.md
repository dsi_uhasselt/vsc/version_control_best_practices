# git client software

Depending on your operating system and workflow, you have a number
of choices to interact with remote git repositories on, e.g.,
GitLab.


## Cross-platform & GUI

SmartGit is a nice git client with a graphical user interface.  It
runs on Windows, MacOS and Linux.  It is free for academic use.
[https://www.syntevo.com/smartgit/](https://www.syntevo.com/smartgit/)


## Linux

If you're a Linux user, you are most likely familiar with the
bash command line, and the command line git implementation will
serve you quite well.

A few software packages can make your life a bit easier though.

  1. One of my favorites is git-prompt that adds the current branch
     to your prompt when you are in a repository directory.  It will
     also display some information on the state (e.g., merge).
     It can be used by modifying the `PS1` environment variable
     appropriately.  The software is available on:
     [https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh).
  1. To more conveniently view git logs, you may want to use
     tig, available at
     [https://github.com/jonas/tig](https://github.com/jonas/tig).
  1. git functionality can be integrated into various editors, including
     vim.  The fugative plugin does a nice job, see
     [https://github.com/tpope/vim-fugitive](https://github.com/tpope/vim-fugitive)


## Windows

git can be used on the command line in Windows, either using the
old school command prompt or the PowerShell.  You can download and
install the Windows implementation from the git website:
[https://git-scm.com/](https://git-scm.com/)

Although developed with GitHub integration in mind, the GitHub
GUI client software can also be used with GitLab.  It can be
downloaded from
[https://desktop.github.com/](https://desktop.github.com/)


## MacOS

Since MacOS has a terminal emulator, you can check out the
information on Linux, since it applies to MacOS as well.
