# Jupyter notebooks and version control

Jupyter notebooks contain a lot of meta-information such as the exact version
of Python it was last executed with, the number of times a cell was executed
and so on.  This makes it virtually impossible to use diff to compare versions
of a notebook, and renders git partially useless.

Fortunately, there is a way around this: jupytext.  It is a command line
utility to convert jupyter notebooks to and from various markup formats such
as Markdown.  It also comes with a jupyter notebook/jupyter lab extension that
allows you to pair a notebook and a markup file.  The markup version a
stripped-down version of the notebook's contains.  It has the markdown cells
of the notebook as markdown, and the code cells as code fragments.  The output
is stripped though, and very little meta-information is stored.

This paired markup file can be kept under version control rather than the
notebook.  Since it has a very simple format, tools like diff work flawlessly.


## Installing  jupytext

The installation of jupytext is straightforward when using conda to  manage
our Python environments.  Say you would like to add jupytext to the
environment `science` that already contains `jupyter` and all the other
Python packages your project requires.

Activate your environment:
```bash
$ conda activate science
```

Install jupytext:
```bash
$ conda install -c conda-forge jupytext
```

The extension for jupyter notebook and jupyter lab is installed
automatically, although you will be prompted for a rebuild by the latter the
first time you start jupyter lab.


## Usage

Jupytext is integrated somewhat differently in jupyter notebooks and jupyter
lab, but the principle is the same.  You only have to do this once.


### Jupyter notebook

1. Open the notebook file you want to work with.
1. In the `File` menu, you will notice a new menu item 'Jupytext`, from the
   submenu, select `Pair notebook with Markdown`.

That's it.  When you save the notebook, a file with the same name, but
extension `.md` will be created.

The jupytext extension ensures that Markdown files have a jupyter notebook
icon, and you can simply opening it by double-clicking as you would a
notebook.


### Jupyter lab

1. Open the notebook file you want to work with.
1. In the command pallet there is a new section `JUPYTEXT` (you have to
scroll down quite a bit), check `Pair Notebook with Markdown`.

That's it.  When you save the notebook, a file with the same name, but
extension `.md` will be created.

The jupytext extension ensures that you can right-click a Markdown file
and open it with a notebook.
