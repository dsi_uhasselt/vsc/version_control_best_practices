# What should go into a repository?

By definition, things for which the history matters, i.e.,
for which you want to keep track of changes.

Things that definitely go into a repository:

  * source code files;
  * build scripts (e.g., make files);
  * (small) data files for testing.
  * text files document the project;
  * LaTeX source of publications;
  * presentations

In general, don't store files that can be reproduced by running
software, for example

  * output data of your application;
  * artifacts of a compiler (`.o`, `.so`, `.a`, `__pycache__`
    and such);
  * DVI or PDF files generated from LaTeX source.

Of course, it is good practice to store target output when it is
part of a test for your software.


## Is everything public?

Not necessarily, you can keep a repository private, i.e.,
the owner and, optionally, designated administrators control
who has access to a repository.

Note however that you can not have a repository that is partly
private, partly public.  In a private repository you do have
fine-grained control over who can take which actions.  You can
allow some people to only view files, but not add or change files.

You can change the status of a repository from private to
public or vice versa at any time.  Hence a typical scenario
could be that while working on a publication, the information
is stored in a private repository that is made public once the
publication is accepted.

Everyone can view files and their history in a public repository,
but changes can only be made by designated users (controlled by
the owner/administrators).


## Which file types can I store in a repository?

There are not restriction on file types, but note that many
features of a version control system work with text files,
but not binary files.  For example, viewing differences between
versions of files, or merging changes in files.

Also, git saves space by storing differences between versions,
rather than entire files, and this is very likely to work out
poorly for binary files, in the sense that differences will be
very large (there's no semantics!).  For largish binary files,
this may make your repository's size grow quite quickly.


## What about my data?

Small data sets that you use to test your software should probably
go into the repository, even if they'll never change or are in a
binary format.  This makes your repository self-contained, which
is a big bonus.

Storing production data in a version control system would be
nice in some scenarios, but there are some practical limitations.
For GitLab, the maximum size of a repository is 10 GB, so that
imposes a strict upper limit.

Also keep in mind that a large repository will take a long time to
clone, which you may want to avoid.


## What about configuration files?

It depends.  If it makes sense to share configuration files, i.e.,
if not every team member needs to make personal modifications it makes
sense.  For instance, the `.gitignore` file that defines things that
should never be under version control in your repository should go
into the repository.

However, be very careful when configuration files contain user
credentials such as passwords or access tokens.  You most likely
don't want to share those with the world.


## What about Microsoft Office documents?

Microsoft office documents are (or can be considered as) binary
files, and hence the usual git workflow for files types such
as Microsoft Word, Excel or PowerPoint does not apply, and
one has to be cautious when collaborating on such documents.
Concurrent edits will have to be resolved by using Microsoft
functionality in the respective applications.

Bottom line: it can be done (and I do), but there be dragons.
