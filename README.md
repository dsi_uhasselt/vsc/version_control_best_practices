# Version control best practices

This project tries to list some best practices and tips and tricks for using
version control systems effectively.

It will be focused on using GitLab since this is what we intend to use in DSI.
GitLab relies on git on the client side.

This site is deployed as GitLab Pages at:
https://dsi_uhasselt.gitlab.io/vsc/version_control_best_practices/
